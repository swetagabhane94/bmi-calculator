import React, { Component } from "react";
import Logo from "../BMI+Scale.jpg"

class BodyMassIndex extends Component {
  constructor() {
    super();
    this.state = {
      height:'',
      weight: '',
      result:0,
      bmiType:''
    };
 }

  updateHeight=(event) =>{
    this.setState(() => {
      return {
        height: event.target.value,
      };
    });
  }

  updateWeight=(event)=> {
    this.setState(() => {
      return {
        weight: event.target.value,
      };
    });
  }

  calculateBMI=()=> {
    let weight = this.state.weight;
    let height = this.state.height/100; 

    let calculate = (weight/(height * height)).toFixed(1);
    this.setState(()=>{
        let dataBMI=this.getBmi(calculate);  
        return {
            result:calculate,
            height:'',
            weight:'',
            bmiType:dataBMI       
        }
    })
  }

  getBmi(bmi) {
    if (bmi < 18.5) {
      return "underweight";
    }
    else if (bmi >= 18.5 && bmi < 24.9) {
      return "normal weight";
    }
    else if (bmi >= 25 && bmi < 29.9) {
      return "overweight";
    }
    else if(bmi >= 30) {
      return "obesity";
    }
  }

  render() {
    return (
    <div>
      <img src={Logo} alt=""></img>
      <div className="container">
        <div className="inputField">
          <h3>Height(in cm)</h3>
          <input
            onChange={this.updateHeight}
            placeholder="Enter Height in centimeters"
            value={this.state.height}
          ></input>
        </div>

        <div className="inputField">
          <h3>weight(in kg)</h3>
          <input
            onChange={this.updateWeight}
            placeholder="Enter weight in Kg"
            value={this.state.weight}
          ></input>
        </div>

        <button onClick={this.calculateBMI}>Calculate BMI</button>
        <div className="show">
          <p>BMI = {this.state.result}</p>
          <p>You are in the {this.state.bmiType} range.</p>
        </div>
      </div>
    </div>
    

    );
  }
}

export default BodyMassIndex;
